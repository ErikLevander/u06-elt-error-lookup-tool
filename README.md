## Dokumentation för överlämning av pyACEL (Python Automated Code Exception Lookup) ##
Innehållsförteckning:
[Problem](#problem)
[Lösning](#lösning)
[Funktion](#funktion)
[Starta](#starta)
[Variabler](#variabler)
[Sökning](#sökning)
[Databas](#databas)
[StackOverflow](#stackoverflow)
[Koden](#koden)
[Tester](#tester)
[Backlog](#backlog)

----

### Problem
Fel som upptäcks vid exekvering av kod kallas för exception. När man skriver kod och
får exception error tar det onödigt lång tid att felsöka och googla på t.ex. StackOverflow.
<br><br>

### Lösning
Automatiserad felsökning med databas-samt APIkoppling till StackOverflow som listar
felmeddelanden liknande de som upptäckts.
<br><br>

### Funktion
pyACEL försöker köra angiven fil och samlar upp felmeddelanden i olika
variabler. Dessa variabler används för att söka i SQLITE3 databas  eller i StackOverflows databas. Allt sparas i sen den lokala databasen.
Det finns två varianter av pyACEL. Som lokal installation och som Docker container
<br><br>

### Starta
För att läsa information om pyACEL:

    python3 pyacel.py
  
För att starta pyACEL Scan:

    docker build -t pyacel .
    docker run -it --rm pyacel <your_file.py>
    python3 pyacel.py <your_file.py>
  
<your_file> är den fil som ska kontrolleras och måste finnas i samma katalog. För testning av pyACEL finns en fil som heter errors.py
<br><br>

### Variabler
Exempel på error exception vid exekvering av kod:

    Traceback (most recent call last):
    File "main.py", line 25, in <module>
    errors()
    File "main.py", line 2, in errors
    print(hej) #NameError
    NameError: name 'hej' is not defined
Detta medelande bryts upp och sparas i variabler nedan:
- error_in_line: sparar vilken rad felet finns.
- exception_name: sparar typen av exception.
- error_msg: sparar felmeddelandet.
<br><br>

### Sökning
Sökning sker efter exception_name i båda tabellerna.
Eventuell träff visas automatiskt i terminalen:

    (1) Checking file main.py..
    (2)EXCEPTION ERROR FOUND IN LINE 2, -> NameError: name 'hej' is not defined.
    Found this in local database:
    (3)[NameError, Raised when a variable is not found in local or global scope.]
    URL about NameError:
    (4)https://stackoverflow.com/questions/21122540/input-error-nameerror-name-is-not-defined

Förklaringar:
1. Visar vilken fil som skannas.
2. Visar ev. exception error som hittats.
3. Visar information om ovan nämnda exception error (från tabellen exceptions).
4. Visar klickbar länk med liknande fel på Stack Overflow (från tabellen errors).

Efter att ha rättat felet och sparat filen, räcker det att återigen välja:

    1.Repeat last scan
<br><br>

### Databas
Databasen heter db_pyacel.db. Den innehåller två tabeller: exceptions och errors.

- I exceptions finns posten tag som innehåller de taggar som används vid sökning på
StackOverflow. Ändra på taggarna för att få annorlunda sökningsresultat.

- I errors finns posterna ‘question_id’, ‘title’ och ‘link’. Dessa kommer från StackOverflows API
response.

- ‘exception_name’ är PRIMARY KEY i båda tabellerna och även FOREIGN KEY i tabellen
errors. Det är 1:1-samband mellan exceptions och errors.


För att skapa en ny databas: 

    cat u06_schema_seeds.sql | sqlite3 db_pyacel.db


Om edentity i databasen är äldre än 6 månader tas den bort.
<br><br>

### StackOverflow
Maxgräns på antalsökningar per dygn: 300

Vid sökning på StackOverflow listas det högst rankade svaret enligt nedan:
1. Sort: relevance
2. Order: desc
   
För att förbättra eller förändra på API-sökningen:
- Url:
https://api.stackexchange.com/docs/advanced-search#page=1&pagesize=3&order=desc&sort=relevance&q=SyntaxError&agged=python%3Bsyntax-error&filter=default&site=stackoverflow

- Ändra på sökkriterierna och kopiera länken till pyacel.py rad 145.
- Byt ut q till {exception_name} och tagged till {tag}
<br><br>

### Koden
Det finns tre klasser: 
- Database skapar själva uppkopplingen till databasen.
- Inspection bearbetar filen och felmeddelanden. Dockstrings finns under varje funktion som förklarar vad som görs.
- Menu visar meny och exekverar kod som är kopplade till valen i menyn. Dockstrings finns under varje funktion som vilket val användaren har valt.
<br><br>

### Tester
PEP8 och flake8 är testade och godkända i Gitlab ci.
<br><br>

### Referenser
API StackoverFlow
Inga inloggningsuppgifter krävs. Antal sökningar per dygn max 300.
https://api.stackexchange.com/docs/advanced-search#page=1&pagesize=3&order=desc&sort=relevance&q=SyntaxError&agged=python%3Bsyntax-error&filter=default&site=stackoverflow

SQLITE3 databas.
Inga inloggningsuppgifter krävs


### Backlog
Du gör såklart vad du vill med koden. Men här är ett förslag på backlog.
- Nya menyer
- 'Docker run pyacel' fungerar ej (för att se info screen i docker container)
- Datbasen godkänner bra ett error per exception. Kanske behövs fler errors för varje exception_name
- Kontoll att timestamp inte är äldre än 6 månader finns inte än.
- IndexError: list index out of range när errors.py är helt tom
- Listar bara ett svar från StackOverflow. Bättre med en lista på 3-4 alternativ.
- Menyval 4. Modify this exception fungerar inte
- IndentationError: unexpected indent fångas inte upp av funktionen find_exception() Fattar inte varför…
- Det finns inga coverage- eller unittester än.
