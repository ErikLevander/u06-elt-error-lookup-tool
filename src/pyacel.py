# Chas Academy 2022. Kjell Bovin 2022-04-01

import datetime
import json
import os
import sqlite3
import sys
import traceback

import requests


class Database:
    def __init__(self):
        self._conn = sqlite3.connect('db_pyacel.db')
        self._cursor = self._conn.cursor()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    @property
    def connection(self):
        return self._conn

    @property
    def cursor(self):
        return self._cursor

    def commit(self):
        self.connection.commit()

    def close(self, commit=True):
        if commit:
            self.commit()
        self.connection.close()

    def execute(self, sql, params=None):
        self.cursor.execute(sql, params or ())

    def fetchall(self):
        return self.cursor.fetchall()

    def fetchone(self):
        return self.cursor.fetchone()

    def query(self, sql, params=None):
        self.cursor.execute(sql, params or ())
        return self.fetchall()


class Inspection:

    def __init__(self):
        self.your_file = sys.argv[1]

    def find_exception(self):
        '''Attempts to execute the file. Narrows all error messages into different variables.
         Searches for error messages in Sqlite3 database and prints them'''

        try:
            exec(open(self.your_file).read())
            print('No error exceptions found. Its all good!')
            self.exception_name = ""

        except Exception as ex:
            self.exception_name = type(ex).__name__
            self.error_msg = traceback.format_exc().splitlines()[-1]
            self.error_in_line = traceback.format_exc().splitlines()[-2].split(' ')[5]
            self.reg_date = datetime.datetime.now()
            print(f"EXCEPTION ERROR FOUND IN LINE {self.error_in_line} -> {self.error_msg}.\n")

        with Database() as db:
            comments = db.query("""
                        SELECT ex.exception_name, ex.cause_of_error, er.link
                        FROM exceptions ex
                        LEFT JOIN errors er ON ex.exception_name = er.exception_name
                        WHERE er.exception_name=?""", (self.exception_name,))

        if comments:
            print('Found this in local database:')
            print(f'[{comments[0][0]}, {comments[0][1]}]\nURL about {comments[0][0]}: {comments[0][2]}')
        else:
            print(f">>Can not find '{self.exception_name}' in local database.<<\n Please follow these two steps:\n(1) Select option 2 to search in Stack Overflow.\n(2) Select option 5 to add the exception to the database.")

    def check_file(self):
        '''If the file exists, find_exception() is executed'''
        file_list = os.listdir('.')
        if self.your_file not in file_list:
            print("Can not find file. Must be in same directory and spelled correctly. Try again. Feel the force!")
            sys.exit(0)
        cls()
        print(f'Checking file {self.your_file}..')
        self.find_exception()


class Menu:
    def __init__(self):

        self.choices = {
            "1": self.repeat_scan,
            "2": self.search_onSO,
            "3": self.show_all_exceptions,
            "4": self.modify_exception,
            "5": self.add_exception,
            "6": self.about,
            "qq": self.quit
        }

    def display_menu(self):

        print("---------------------------------------------PyACEL Menu-----------------------------------------------")
        print("""1.Repeat last scan         2.Search for this exception in StackOverflow    3.Show All errors """)
        print("""4.Modify this exception    5.Add this exception to database                6.About       (qq.Quit)""")

    def run(self):
        while True:
            self.display_menu()
            choice = input("Enter an option: ")
            action = self.choices.get(choice)
            if action:
                action()
            else:
                cls()
                print("{0} is not a valid choice".format(choice))

    def repeat_scan(self):
        '''1.Repeat last scan'''
        main()

    def search_onSO(self):
        '''2.Select sorting order and search for exception in StackOverflow'''
        cls()
        ins = Inspection()
        ins.find_exception()
        ins.exception_name
        ins.error_msg

        print('''Select sorting order in StackOverflow: \n
Activity - last activity date
Creation - creation date
Votes - score
Relevance - matches the relevance tab on the site itself
        ''')
        self.sort = input('Type sort to change sorting order(press Enter to use default): ')
        if not self.sort:
            self.sort = 'Relevance'

        with Database() as db:
            comments = db.query("""SELECT tag
                        FROM exceptions WHERE exception_name=?""", (ins.exception_name,))
            tag = comments[0][0]

        url = f'https://api.stackexchange.com/2.3/search/advanced?page=1&pagesize=3&order=desc&sort={self.sort}&q={ins.exception_name}&tagged=python;{tag}&site=stackoverflow'
        response = requests.get(url)
        self.data = (json.loads(response.text))
        # print(self.data)

        print('\nFound this in Stack Overflow:')
        print(f'Sorting order: {self.sort}')
        print(self.data['items'][0]['link'])

    def show_all_exceptions(self):
        '''3.Show All Exceptions'''
        cls()
        with Database() as db:
            comments = db.query("""SELECT * FROM errors""")
            for item in comments:
                print(item)

    def modify_exception(self):
        '''4.Modify this Exception'''
        pass

    def add_exception(self):
        '''5.Add this exception to database'''
        cls()
        ins = Inspection()
        ins.find_exception()
        da = Menu()
        da.search_onSO()
        print('Database has been successfully updated!')
        self.data = da.data
        self.exception_name = ins.exception_name
        self.question = ins.error_msg
        self.so_qustion_id = self.data['items'][0]['question_id']
        self.title = self.data['items'][0]['title']
        self.link = self.data['items'][0]['link']
        self.reg_date = ins.reg_date
        self.error_list = [self.exception_name, self.question, self.so_qustion_id, self.title, self.link]

        with Database() as db:
            comments = db.query("""
                        INSERT INTO errors(
                        exception_name,
                        question,
                        question_id,
                        title, link
                        )
                        VALUES (?,?,?,?,?)""", (self.error_list))
            print(comments)

    def about(self):
        '''6.About'''
        cls()
        print("""Welcome to PyACEL (Python Automated Code Exception Lookup)
Errors detected during execution are called exceptions.
PyACEL checks exceptions in pythonfiles and automatically
searches for the most common solution in local database and StackOverflow.

To start a scan: python3 pyacel.py <your_file.py>\n\n""")

    def quit(self):
        print("Thank you for using PyACEL today. Good bye.")
        sys.exit(0)


def cls():
    '''To easy preform a clear screen in both Linux(clear) and Windows(cls)'''
    print('\n' * 100)


def main():
    '''If the the file to be checked has been specified, check_file() is executed. Otherwise about() is displayed'''
    print(sys.argv)
    if len(sys.argv) == 1:
        Menu().about()
        sys.exit(0)
    else:
        Inspection().check_file()
        Menu().run()


if __name__ == "__main__":
    main()
