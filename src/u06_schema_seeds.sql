PRAGMA foreign_keys = ON;

CREATE TABLE IF NOT EXISTS exceptions 
            (
            exception_name TEXT PRIMARY KEY,
            tag TEXT,
            cause_of_error TEXT
            );

CREATE TABLE IF NOT EXISTS errors
        (
        exception_name TEXT PRIMARY KEY,
        question TEXT,
        question_id INT,
        title TEXT,
        link TEXT,
        reg_date DATE NOT NULL DEFAULT current_timestamp,
        FOREIGN KEY(exception_name) REFERENCES exceptions(exception_name)
        );



INSERT INTO exceptions 
        (exception_name, tag, cause_of_Error) VALUES
        ('AssertionError','assert','Raised when an assert statement fails.'),
        ('AttributeError','attributes','Raised when attribute assignment or reference fails.'),
        ('EOFError','eoferror','Raised when the input() function hits end-of-file condition.'),
        ('FloatingPointError','floating-point','Raised when a floating point operation fails.'),
        ('GeneratorExit','generator','Raise when a generators close() method is called.'),
        ('ImportError','import','Raised when the imported module is not found.'),
        ('IndexError','index-error','Raised when the index of a sequence is out of range.'),
        ('KeyError','keyerror','Raised when a key is not found in a dictionary.'),
        ('KeyboardInterrupt','keyboardinterrupt','Raised when the user hits the interrupt key (Ctrl+C or Delete).'),
        ('MemoryError','out-of-memory','Raised when an operation runs out of memory.'),
        ('NameError','nameerror','Raised when a variable is not found in local or global scope.'),
        ('NotImplementedError','notimplementedexception','Raised by abstract methods.'),
        ('OSError','oserror', 'Raised when system operation causes system related error.'),
        ('OverflowError','overflowexception','Raised when the result of an arithmetic operation is too large to be represented.'),
        ('ReferenceError','reference','Raised when a weak reference proxy is used to access a garbage collected referent.'),
        ('RuntimeError','runtime-error','Raised when an error does not fall under any other category.'),
        ('StopIteration','stopiteration','Raised by next() function to indicate that there is no further item to be returned by iterator.'),
        ('SyntaxError','syntax-error','Raised by parser when syntax error is encountered.'),
        ('IndentationError','indentation','Raised when there is incorrect indentation.'),
        ('TabError','indentation','Raised when indentation consists of inconsistent tabs and spaces.'),
        ('SystemError','os.system','Raised when interpreter detects internal error.'),
        ('SystemExit','systemexit','Raised by sys.exit() function.'),
        ('TypeError','typeerror','Raised when a function or operation is applied to an object of incorrect type.'),
        ('UnboundLocalError','unbound','Raised when a reference is made to a local variable in a function or method, but no value has been bound to that variable.'),
        ('UnicodeError','unicode','Raised when a Unicode-related encoding or decoding error occurs.'),
        ('UnicodeEncodeError','unicode','Raised when a Unicode-related error occurs during encoding.'),
        ('UnicodeDecodeError','unicode','Raised when a Unicode-related error occurs during decoding.'),
        ('UnicodeTranslateError','unicode','Raised when a Unicode-related error occurs during translating.'),
        ('ValueError','valueerror','Raised when a function gets an argument of correct type but improper value.'),
        ('ZeroDivisionError','divide-by-zero','Raised when the second operand of division or modulo operation is zero.'),
        ('ModuleNotFoundError','python-import','raised when Python cannot successfully import a module.')  














