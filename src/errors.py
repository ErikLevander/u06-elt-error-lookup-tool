print(hej) #NameError

#1+1 = 4 #SyntaxError
    
#y = 0 ; assert y != 0 #AssertionError
    
#import solsken #ModuleNotFoundError
    
#print('Hello') 
#    print('World') #Indentation error

#list = [1] ; print (list[2]) #IndexError
    
#a = 1 + 'b' #TypeError

#import math
#print(math.sqrt(-1)) #ValueError

#a= 5/0 #ZeroDivisionError